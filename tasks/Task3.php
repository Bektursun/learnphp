<?php

// Упражнение 3. Функция ereg_replace является устаревшей, замените ее на preg_replace
// preg_replace — Выполняет поиск и замену по регулярному выражению

if (preg_replace("/PHP([3-6])/", "PHP7", "PHP6")) {
    echo "Next step!";
}

$string = "November 26, 1997";
$pattern = '/(\w+) (\d+), (\d+)/i';

$replacement = '${1}1,$3';
echo preg_replace($pattern, $replacement, $string);

$someString = "The quick brown fox jumps over the lazy dog.";
$patterns = array();

$patterns[0] = '/quick/';
$patterns[1] = '/brown/';
$patterns[2] = '/fox/';
$patterns[3] = '/dog/';

$replacements = array();
$replacements[0] = 'bear';
$replacements[1] = 'black';
$replacements[2] = 'slow';
$replacements[3] = 'cat';

echo preg_replace($patterns, $replacements, $someString);


